<?php include("header.php");?>

<section style="background-color: #eee;">
        <div class="text-center container py-5">
            <!-- <div class="container py-5"> -->
          <!-- <h4 class="mt-4 mb-5"><strong>Bestsellers</strong></h4> -->
          <div class="text-center mx-auto wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px;">
              <h1 class="display-6">My Account</h1>
              <p class="text-primary fs-5 mb-5">Lorem ipsum dolor asmet.</p>
          </div>
      
          <div class="row">
            <div class="col-lg-8 col-md-8 offset-lg-2 offset-md-2 offset-0 col-12 mb-4 wow fadeInUp" data-wow-delay="0.1s">
                
                <div class="card">
                
                    <div class="card-body">
                    
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Profile</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Change Password</button>
                            </li>
                        </ul>

                        <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active " id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <form action="" class="mt-5">
                                <div class="col-lg-12 col-md-12 mb-3" >
                                    <div class="row g-3">
                                        <div class="col-12">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="full_name" value="Andi Saputra" name="full_name" placeholder="Full Name" required>
                                                <label for="name">Full Name*</label>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-floating">
                                                <input type="email" class="form-control" id="email" name="email" value="andisaputra@gmail.com" placeholder="Email*" required>
                                                <label for="email">Email*</label>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="whatsapp" name="whatsapp" value="+6287712344321" placeholder="WhatsApp Number" required>
                                                <label for="subject">WhatsApp Number*</label>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="form-floating">
                                                <textarea type="text" class="form-control" id="address" style="height: 90px;" name="address" placeholder="Address" required></textarea>
                                                <label for="subject">Address</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-grid gap-2 mt-5">
                                    <button href="paket-detail.php" class="btn btn-primary btn-lg" type="submit">
                                    <!-- <i class="fa fa-money"></i> -->
                                    <i class="far fa-save"></i>
                                        Save
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                            <form action="" class="mt-5">
                                <div class="col-lg-12 col-md-12 mb-3" >
                                    <div class="row g-3">
                                        <div class="col-12">
                                            <div class="form-floating">
                                                <input type="password" class="form-control" id="current_pasword" value="" name="current_pasword" placeholder="Current Password" required>
                                                <label for="name">Current Password*</label>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-floating">
                                                <input type="password" class="form-control" id="new_pasword" name="new_pasword" placeholder="New Password" required>
                                                <label for="email">New Password*</label>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-floating">
                                                <input type="password" class="form-control" id="conf_new_pasword" name="conf_new_pasword" value="" placeholder="Confirmation New Password" required>
                                                <label for="subject">Confirmation New Password*</label>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="d-grid gap-2 mt-5">
                                    <button href="paket-detail.php" class="btn btn-primary btn-lg" type="submit">
                                    <!-- <i class="fa fa-money"></i> -->
                                    <i class="far fa-save"></i>
                                        Save
                                    </button>
                                </div>
                            </form>
                        </div>
                        </div>

                    </div>
                </div>

            </div>
      
          </div>
        </div>
        <!-- </div> -->
      </section>
<?php include("footer.php");?>