<?php include("header.php");?>

<section style="background-color: #eee;">
        <div class="text-center container py-5">
            <!-- <div class="container py-5"> -->
          <!-- <h4 class="mt-4 mb-5"><strong>Bestsellers</strong></h4> -->
          <div class="text-center mx-auto wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px;">
              <h1 class="display-6">Payment Success</h1>
              <p class="text-primary fs-5 mb-5">Thank you for buying in our store.</p>
          </div>
      
          <div class="row">
            <div class="col-lg-6 col-md-6 offset-lg-3 offset-md-3 offset-0 col-12 mb-4 wow fadeInUp" data-wow-delay="0.1s">
                <i class="fas fa-star mb-5" style="color: #ffea00;font-size: 100px;"></i>
                <div class="card">
                
                    <div class="card-body">
                        <h2 class="mb-0">
                            <span id="token">GH1JK639CV</span>
                            <small><a href="javascript:void(0)" id="copyToClipboard" onclick="copyToClipboard('#token');" style="position: absolute;right: 10px;">Copy</a></small>
                        </h2>                        
                    </div>
                </div>

                <div class="text-start py-5">
                    <p>
                        <h5>Instructions for Using Tokens</h5>
                    </p>

                    <p>
                    Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit. Aliqu diam amet diam et eos. Clita erat ipsum et lorem et sit, sed stet lorem sit clita duo justo magna dolore erat amet
                    </p>

                </div>
            </div>
      
          </div>
        </div>
        <!-- </div> -->
      </section>
<?php include("footer.php");?>