<?php include("header.php");?>

<section style="background-color: #eee;">
        <div class="text-center container py-5">
            <!-- <div class="container py-5"> -->
          <!-- <h4 class="mt-4 mb-5"><strong>Bestsellers</strong></h4> -->
          <div class="text-center mx-auto wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px;">
              <h1 class="display-6">My Order</h1>
              <p class="text-primary fs-5 mb-5">Your Transaction Histories.</p>
          </div>
      
          <div class="row">
            <div class="col-lg-8 col-md-8 offset-lg-2 offset-md-2 offset-0 col-12 mb-4 wow fadeInUp" data-wow-delay="0.1s">
                
                <div class="card">
                
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="table-dark">
                                    <tr>
                                        <th>No</th>
                                        <th>Order Date</th>
                                        <th>Payment Status</th>
                                        <th>Active Period</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td><a href="order-detail.php?status=paid">20 Jul 2023</a></td>
                                        <td>Paid</td>
                                        <td>3 Months Left</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td><a href="order-detail.php">25 Jul 2023</a></td>
                                        <td>Unpaid</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td><a href="order-detail.php?status=paid">20 Aug 2023</a></td>
                                        <td>Paid</td>
                                        <td>1 Months Left</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>
      
          </div>
        </div>
        <!-- </div> -->
      </section>
<?php include("footer.php");?>