<?php include("header.php");?>

    <section style="background-color: #eee;">
        <div class="text-center container py-5">
      
          <div class="row">
            <?php $datas = ['A'];
            foreach ($datas as $key => $paket) {
            ?>
            <div class="col-lg-8 offset-lg-2 col-sm-12 offset-sm0 mb-4 wow fadeInUp" data-wow-delay="0.1s">
              <div class="card">
                <div class="bg-image hover-zoom ripple ripple-surface ripple-surface-light"
                  data-mdb-ripple-color="light">
                  <img src="img/detail-product1.webp"
                    class="w-100" />
                  <a href="#!">
                    <div class="mask">
                      <div class="d-flex justify-content-start align-items-end h-100">
                        <h5><span class="badge bg-primary ms-2">New</span> <span class="badge bg-danger ms-2">-10%</span></h5>
                      </div>
                    </div>
                    <div class="hover-overlay">
                      <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                    </div>
                  </a>
                </div>
                <div class="card-body">
                    <p class="text-start py-3">
                    Dolor nonumy tempor elitr et rebum ipsum sit duo duo. Diam sed sed magna et magna diam aliquyam amet dolore ipsum erat duo. Sit rebum magna duo labore no diam.

                    Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit. Aliqu diam amet diam et eos. Clita erat ipsum et lorem et sit, sed stet lorem sit clita duo justo magna dolore erat amet


                    </p>
                    <a href="paket-detail.php" class="text-reset py-5">
                        <h5 class="card-title mb-3">Buying Package <?php echo $paket;?></h5>
                    </a>
                  <h6 class="mb-3">
                    <s>Rp 70.000</s><strong class="ms-2 text-danger">Rp 60.000</strong>
                  </h6>
                  

                        
                    <form action="https://app.sandbox.midtrans.com/payment-links/1692590033642">
                        <div class="col-lg-12 col-md-12 mb-3" >
                            <div class="row g-3">
                                <div class="col-12">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" id="full_name" value="Andi Saputra" name="full_name" placeholder="Full Name" required>
                                        <label for="name">Full Name*</label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-floating">
                                        <input type="email" class="form-control" id="email" name="email" value="andisaputra@gmail.com" placeholder="Email*" required>
                                        <label for="email">Email*</label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" id="whatsapp" name="whatsapp" value="+6287712344321" placeholder="WhatsApp Number" required>
                                        <label for="subject">WhatsApp Number*</label>
                                    </div>
                                </div>


                                <div class="col-12">
                                    <div class="text-start">
                                        <input type="checkbox" class="form-controlx" id="create_accoung" name="create_accoung" required checked />
                                        <label for="subject">Create Account</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="d-grid gap-2 mt-5">
                            <button href="paket-detail.php" class="btn btn-primary btn-lg" type="submit">
                            <!-- <i class="fa fa-money"></i> -->
                            <i class="far fa-money-bill-alt"></i>
                                Pay Now
                            </button>
                        </div>
                    </form>
                </div>
                

              </div>
            </div>
            <?php }?>
          </div>
        </div>
        <!-- </div> -->
      </section>
<?php include("footer.php");?>