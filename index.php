<?php include("header.php");?>


      <!-- Header Start -->
      <div class="container-fluid hero-header bg-light py-5 mb-5">
          <div class="container py-5">
              <div class="row g-5 align-items-center">
                  <div class="col-lg-6">
                      <h1 class="display-4 mb-3 animated slideInDown">Make Better Life With Trusted</h1>
                      <p class="animated slideInDown">Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit. Aliqu
                          diam amet diam et eos. Clita erat ipsum et lorem et sit, sed stet lorem sit clita duo justo
                          magna dolore erat amet</p>
                      <a href="" class="btn btn-primary py-3 px-4 animated slideInDown">Explore More</a>
                  </div>
                  <div class="col-lg-6 animated fadeIn">
                      <img class="img-fluid animated pulse infinite" style="animation-duration: 3s;" src="img/img-hero1.png"
                          alt="">
                  </div>
              </div>
          </div>
      </div>
      <!-- Header End -->


      <section style="background-color: #eee;">
        <div class="text-center container py-5">
            <!-- <div class="container py-5"> -->
          <!-- <h4 class="mt-4 mb-5"><strong>Bestsellers</strong></h4> -->
          <div class="text-center mx-auto wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px;">
              <h1 class="display-6">Products</h1>
              <p class="text-primary fs-5 mb-5">Lorem ipsum dolor asmet.</p>
          </div>
      
          <div class="row">
            <?php $datas = ['A','B','C','D','E','F'];
            foreach ($datas as $key => $paket) {
            ?>
            <div class="col-lg-4 col-md-6 col-sm-6 mb-4 wow fadeInUp" data-wow-delay="0.1s">
              <div class="card">
                <div class="bg-image hover-zoom ripple ripple-surface ripple-surface-light"
                  data-mdb-ripple-color="light">
                  <img src="img/product1.jpg"
                    class="w-100" />
                  <a href="#!">
                    <div class="mask">
                      <div class="d-flex justify-content-start align-items-end h-100">
                        <h5><span class="badge bg-primary ms-2">New</span> <span class="badge bg-danger ms-2">-10%</span></h5>
                      </div>
                    </div>
                    <div class="hover-overlay">
                      <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                    </div>
                  </a>
                </div>
                <div class="card-body">
                  <a href="paket-detail.php" class="text-reset">
                    <h5 class="card-title mb-3">Package <?php echo $paket;?></h5>
                  </a>
                  <a href="" class="text-reset">
                    <p>Category</p>
                  </a>
                  <h6 class="mb-3">
                    <s>Rp 70.000</s><strong class="ms-2 text-danger">Rp 60.000</strong>
                  </h6>
                
                  <div class="d-grid gap-2"><a href="paket-detail.php" class="btn btn-primary" type="button">Detail</a></div>
                </div>
                

              </div>
            </div>
            <?php }?>
          </div>
        </div>
        <!-- </div> -->
      </section>
<?php include("footer.php");?>